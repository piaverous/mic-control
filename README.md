# Mic Control
![](./assets/screencap.gif)

## Description
This project was born out of a need I felt when using stuff 
like FaceTime or Google Hangouts. It just pissed me off to have 
to have to go back to the window containing the app I was using
in order to be able to mute my microphone.

I wanted to be able to do it from anywhere.

This is why I built this super simple tool, that allows you to 
mute your mic from the status bar.
> Note: Only works with MAC as it relies on AppleScript

## Installing

Download the latest release from [https://gitlab.com/piaverous/mic-control/releases](https://gitlab.com/piaverous/mic-control/releases).

### From source
If you wish to install from source, or to dev:

- `cd mic-control`
- `python3 -m venv venv` // optional, only needed if you want to use a virtual environment to build the project
- `pip install -r requirements.txt` // install dependencies
- `python setup.py py2app` // build the app using `py2app`
- `open -a dist/main.app` // run the app

You can then rename the `main.app` to your liking and add it to your applications folder.

## Contributors
- Just me for now, cheers 🍻
