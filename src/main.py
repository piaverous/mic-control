import rumps
from subprocess import Popen, PIPE


def run_command(cmd):
    p = Popen(['osascript', '-'], stdin=PIPE, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    stdout, stderr = p.communicate(cmd)
    return p.returncode, stdout, stderr


def toggle_mic_mute():
    toggle_mic_command = '''if input volume of (get volume settings) = 0 then
        set level to 100
    else
        set level to 0
    end if

    set volume input volume level'''
    run_command(toggle_mic_command)


def is_mic_on():
    cmd = '''if input volume of (get volume settings) = 0 then
        error "Mic is off"
    end if'''
    return_code, stdout, stderr = run_command(cmd)
    return return_code == 0


def get_icon():
    return "🎙" if is_mic_on() else "🔇"


class MicrophoneTrayIcon(rumps.App):

    def __init__(self, name):
        super(MicrophoneTrayIcon, self).__init__(name)
        self.title = get_icon()

    @rumps.clicked("Mute")
    def prefs(self, sender):
        toggle_mic_mute()
        sender.state = not is_mic_on()
        self.title = get_icon();

    @rumps.timer(3)
    def update_icon(self, _):
        self.title = get_icon();


if __name__ == "__main__":
    MicrophoneTrayIcon("🎙").run()
